<?php

function my_sort($original_a,$original_b)
{
   $add_padding_a = str_pad($original_a,2,"0");
   $add_padding_b = str_pad($original_b,2,"0");

   if ($add_padding_a==$add_padding_b){
      //return 0; 
      return $original_a>$original_b?1:-1;
   } 
   else{
      return ($add_padding_a>$add_padding_b) ? 1 : -1;
   }
   
}

$arr = range(1,30);
usort($arr,"my_sort");

echo '<pre>';
print_r($arr);
echo '</pre>';
//shuffle(print_r($arr));
?>